package test;

import java.io.IOException;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import resources.base;

public class Listeners extends base implements ITestListener{

	@Override
	public void onTestStart(ITestResult result) {
//		System.out.println("Test started" );
		
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		  
//		System.out.println("Test completed" );
	}
	@Override
	public void onTestFailure(ITestResult result) {
		
		try {
			getScreenShotPath();
		}	catch (IOException e) {
		
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onTestSkipped(ITestResult result) 
	{
	
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result)
	{
	
	}

	@Override
	public void onTestFailedWithTimeout(ITestResult result) {
		
	}

	@Override
	public void onStart(ITestContext context) {
		
	}

	@Override
	public void onFinish(ITestContext context) {
		
	}
	

}
