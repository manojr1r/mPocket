package test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.io.IOException;
import java.time.Duration;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import pageObjects.HomePage;
import pageObjects.SelectedLinkPage;
import pageObjects.addToCartPage;
import resources.base;

public class HomePageTest extends base {
	public WebDriver driver;

	@AfterSuite
	public void close() {
		driver.quit();
	}

	@BeforeSuite
	public void initialize() throws IOException {
		driver = initializeDriver();
		driver.get(prop.getProperty("url"));
//		driver.manage().window().maximize();
	}

	@DataProvider(name = "sendData")

	public Object[][] rdata() {
		return new Object[][] { { "mobile" } /* , {"phone"}, {"smart phone"} */ };
	}

	@Test(priority = 1, dataProvider = "sendData")

	public void searchItem(String Searchdata) throws IOException, InterruptedException {
		HomePage homepage = new HomePage(driver);
	
		homepage.searchBox().sendKeys(Searchdata);
		homepage.ClicksearchBox().click();

		assertTrue(homepage.searchResults().size() > 0);
	}

	@Test(dependsOnMethods = "searchItem")
	public void addToCart() throws InterruptedException {
		HomePage homepage = new HomePage(driver);
		for (int i = 0; i < homepage.searchResults().size(); i++) {
			if (i == Math.floor((homepage.searchResults().size() + i) / 2)) {
				String pulledName = homepage.searchResults().get(i).getText();
//				System.out.println(pulledName);
				homepage.searchResults().get(i).click();

//				WebDriverWait wait=new WebDriverWait(driver, Duration.ofSeconds(5));
//				wait.until(ExpectedConditions.numberOfWindowsToBe(2));
				Set<String> windows = driver.getWindowHandles();
				Iterator<String> it = windows.iterator();
				it.next();

				String childId = it.next();
				driver.switchTo().window(childId);
				SelectedLinkPage sP = new SelectedLinkPage(driver);
				assertEquals(pulledName, sP.verifyPage().getText());
//				System.out.println("same Item Selected");

				break;
			}
		}

		SelectedLinkPage sP = new SelectedLinkPage(driver);

		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
		wait.until(ExpectedConditions.visibilityOfElementLocated(sP.addToCart));

		sP.addToCart().click();
//		Thread.sleep(2000);
		if (sP.closePopup1().size() > 0) {
			sP.closePopup1().get(0).click();
		}
//		if (sP.closePopup().size() > 0) {
//			sP.closePopup().get(0).click();
//		}
//		

		Thread.sleep(2000);
		wait.until(ExpectedConditions.elementToBeClickable(sP.clickCart()));

		sP.clickCart().click();

	}

	@Test(dependsOnMethods = "addToCart")
	public void quantityTo10() throws InterruptedException {
		addToCartPage addtocart = new addToCartPage(driver);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));

//		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(addtocart.shoppingcart));

		double priceMultipliedOnebyTen = 0;
		double priceForTen = 0;
		if (addtocart.priceForOnes().size() > 0) {
//		wait.until(ExpectedConditions.visibilityOfElementLocated(addtocart.priceForOne));
			addtocart.StaticdropDown().selectByIndex(10);
			addtocart.selectQuantity().sendKeys("10");
			addtocart.updateQuantitybutton().click();

			Thread.sleep(2000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(addtocart.priceForOne));
			String priceForOneinTextsymbol = addtocart.priceForOne().getText();
			String priceForOneinText = priceForOneinTextsymbol.replaceAll(",", "");
			double price = Double.parseDouble(priceForOneinText);
			priceMultipliedOnebyTen = price * 10;
			String priceForTeninText = addtocart.priceForTen().getText();
			priceForTeninText = priceForTeninText.replaceAll(",", "");
			priceForTen = Double.parseDouble(priceForTeninText);
		} else {
			addtocart.StaticdropDown().selectByIndex(10);
			addtocart.selectQuantity().sendKeys("10");

			addtocart.updateQuantitybutton().click();

			Thread.sleep(2000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(addtocart.priceForOne1));
			String priceForOneinText = addtocart.priceForOne1().getText().substring(1);
			priceForOneinText = priceForOneinText.replaceAll(",", "");
			double priceForOne = Double.parseDouble(priceForOneinText);
			priceForOne = (priceForOne) * 10;
			String priceForTeninText = addtocart.priceForTen1().getText().substring(1);
			priceForTeninText = priceForTeninText.replaceAll(",", "");
			priceForTen = Double.parseDouble(priceForTeninText);

		}

//		System.out.println(priceForTen);

		if (((List<WebElement>) addtocart.alert()).size() == 0) {

			SoftAssert softAssertion = new SoftAssert();
			assertEquals(priceMultipliedOnebyTen, priceForTen);
		} else {
			System.out.println("This seller has a limit per customer. check availablity from another seller");
		}

		if (priceMultipliedOnebyTen == priceForTen) {
			System.out.println("Quantity changed to 10 ");
		} else
			System.out.println("Price Mismatch");
//		System.out.println("priceMultipliedOnebyTen " + priceMultipliedOnebyTen + "  priceForTen " + priceForTen);

	}

	@Test(dependsOnMethods = "quantityTo10")
	public void deleteCart() {
		addToCartPage addtocart = new addToCartPage(driver);
		addtocart.delete().click();
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
		wait.until(ExpectedConditions.visibilityOfElementLocated(addtocart.verify));

		assertEquals(addtocart.verify().getText(), "Your Amazon Cart is empty.");
	}

}
